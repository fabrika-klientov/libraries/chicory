<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

class PrintService extends BaseService
{
    protected const PRINT_DECLARATION = 'print/declaration';
    protected const PRINT_REGISTER = 'print/register';
    protected const PRINT_CN23 = 'print/cn23';
    protected const PRINT_STICKER = 'print/sticker';
    protected const PRINT_STICKER100 = 'print/sticker100';
    protected const PRINT_STICKER100A4 = 'print/sticker100A4';

    public function declaration(string $printValue, string $contentType = 'html'): string
    {
        return self::getHttpClient()->getLink(self::PRINT_DECLARATION . "/$printValue/$contentType");
    }

    public function register(string $printValue, string $contentType = 'html'): string
    {
        return self::getHttpClient()->getLink(self::PRINT_REGISTER . "/$printValue/$contentType");
    }

    public function cn23(string $printValue, string $contentType = 'html'): string
    {
        return self::getHttpClient()->getLink(self::PRINT_CN23 . "/$printValue/$contentType");
    }

    public function sticker(string $printValue, string $contentType = 'html', bool $termoPrint = true): string
    {
        $termoPrint = $termoPrint ? 'true' : 'false';

        return self::getHttpClient()->getLink(self::PRINT_STICKER . "/$printValue/$contentType/$termoPrint");
    }

    public function sticker100(string $printValue): string
    {
        return self::getHttpClient()->getLink(self::PRINT_STICKER100 . "/$printValue");
    }

    public function sticker100A4(string $printValue): string
    {
        return self::getHttpClient()->getLink(self::PRINT_STICKER100A4 . "/$printValue");
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
