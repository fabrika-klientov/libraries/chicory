<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Tracking\Responses;
use Illuminate\Support\Collection;

class TrackingService extends BaseService
{
    protected const TRACKING = 'tracking';
    protected const TRACKING_DELIVERED = 'trackingDelivered';
    protected const TRACKING_BY_DATE = 'trackingByDate';
    protected const PARCEL_INFO_TRACKING = 'parcelInfoTracking';
    protected const TRACKING_BY_PERIOD = 'trackingByPeriod';

    public function tracking(string $trackNumber): ?Responses\TrackingResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TRACKING . "/$trackNumber", null),
            Responses\TrackingResponse::class
        );
    }

    /**
     * @param string[] $trackNumbers
     * @return Collection
     * */
    public function trackingList(array $trackNumbers): Collection
    {
        $requests = [];
        foreach ($trackNumbers as $trackNumber) {
            $requests[] = self::doRequest(HttpClient::GET, self::TRACKING . "/$trackNumber", null, [], true);
        }

        if (empty($requests)) {
            return new Collection();
        }

        return self::factoryEntitiesCollect(
            self::doRequestAsync($requests),
            Responses\TrackingResponse::class
        );
    }

    public function trackingDelivered(
        string $dateFrom,
        string $dateTo,
        int $page = 1
    ): ?Responses\TrackingDeliveredResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TRACKING_DELIVERED . "/$dateFrom/$dateTo/$page", null),
            Responses\TrackingDeliveredResponse::class
        );
    }

    public function trackingByDate(string $searchDate): ?Responses\TrackingResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TRACKING_BY_DATE . "/$searchDate", null),
            Responses\TrackingResponse::class
        );
    }

    public function parcelInfoTracking(string $parcelID): ?Responses\ParcelInfoTrackingResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCEL_INFO_TRACKING . "/$parcelID", null),
            Responses\ParcelInfoTrackingResponse::class
        );
    }

    public function trackingByPeriod(string $dateFrom, string $dateTo): ?Responses\TrackingResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TRACKING_BY_PERIOD . "/$dateFrom/$dateTo", null),
            Responses\TrackingResponse::class
        );
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
