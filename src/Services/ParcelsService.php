<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Parcels\Requests;
use Chicory\Entities\Parcels\Responses;
use Chicory\Exceptions\ValidateRequestException;

class ParcelsService extends BaseService
{
    protected const PARCEL_DEBT_INFO = 'parcelDebtInfo';
    protected const PARCEL_STATUS = 'parcelStatus';
    protected const EASY_RETURN_AGENT_INFO = 'EasyReturnAgentInfo';
    protected const PARCEL_STATUS_DETAILS = 'parcelStatusDetails';
    protected const GET_PARCEL = 'getParcel';
    protected const PARCEL_CHANGE_RECEIVER = 'parcelChangeReceiver';
    protected const TIME_SLOT = 'timeSlot';
    protected const PARCEL_CHANGE_CONTRACT_ID = 'parcelChangeContractID';
    protected const PARCEL = 'parcel';
    protected const PARCEL_GEO = 'parcelGeo';
    protected const CALCULATE = 'calculate';
    protected const PARCELS_LIST = 'parcelsList';
    protected const ORDER_DATE_INFO = 'orderDateInfo';
    protected const PACK_TYPES = 'packTypes';
    protected const SPEC_CONDITIONS = 'specConditions';
    protected const INFO_STICKER = 'info4Sticker';
    protected const PHOTO_FIXATION = 'photoFixation';
    protected const PARCEL_GOODS_RETURN = 'parcelGoodsReturn';
    protected const LOCK_PARCEL = 'LockParcel';
    protected const UNLOCK_PARCEL = 'UnlockParcel';

    // TODO: does not work
    public function parcelDebtInfo(
        string $parcelID,
        string $iDAgent,
        bool $byMerchants = true
    ): ?Responses\ParcelDebtInfoResponse {
        $byMerchants = $byMerchants ? 'true' : 'false';

        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCEL_DEBT_INFO . "/$parcelID/$iDAgent/$byMerchants", null),
            Responses\ParcelDebtInfoResponse::class
        );
    }

    // TODO: does not work -> check response
    public function parcelStatus(string $parcelID): ?Responses\ParcelStatusResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCEL_STATUS . "/$parcelID", null),
            Responses\ParcelStatusResponse::class
        );
    }

    public function easyReturnAgents(): ?Responses\EasyReturnAgentsResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::EASY_RETURN_AGENT_INFO, null),
            Responses\EasyReturnAgentsResponse::class
        );
    }

    // TODO: does not work
    public function easyReturnAgentInfo(string $iDAgent): ?Responses\EasyReturnAgentInfoResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::EASY_RETURN_AGENT_INFO, ['IDAgent' => $iDAgent]),
            Responses\EasyReturnAgentInfoResponse::class
        );
    }

    public function parcelStatusDetails(string $parcelID): ?Responses\ParcelStatusDetailsResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCEL_STATUS_DETAILS . "/$parcelID", null),
            Responses\ParcelStatusDetailsResponse::class
        );
    }

    public function getParcel(
        string $parcelID = null,
        string $barCode = null,
        string $parcelNumber = null,
        string $returnMode = 'objectData' // objectData | printCN23 | printCP71
    ): ?Responses\GetParcelResponse {
        $value = $parcelID ?: $barCode ?: $parcelNumber;
        switch (true) {
            case !empty($parcelID):
                $searchMode = 'parcelID';
                break;
            case !empty($barCode):
                $searchMode = 'barCode';
                break;
            case !empty($parcelNumber):
                $searchMode = 'parcelNumber';
                break;
        }

        if (empty($value) || empty($searchMode)) {
            throw new ValidateRequestException('Ones of parameters are required.');
        }

        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::GET_PARCEL . "/$value/$searchMode/$returnMode", null),
            Responses\GetParcelResponse::class
        );
    }

    // TODO: does not work 500 Internal Server Error
    public function parcelChangeReceiver(
        ?string $parcelID,
        Requests\ParcelChangeReceiverRequest $request
    ): ?Responses\ParcelChangeReceiverResponse {
        if (!empty($parcelID)) {
            $request->parcelID($parcelID);
        }

        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::PARCEL_CHANGE_RECEIVER, $request),
            Responses\ParcelChangeReceiverResponse::class
        );
    }

    public function timeSlot(): ?Responses\TimeSlotResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TIME_SLOT, null),
            Responses\TimeSlotResponse::class
        );
    }

    // TODO: does not work
    public function parcelChangeContractID(
        ?string $parcelID,
        ?string $barCode,
        Requests\ParcelChangeContractIDRequest $request
    ): ?Responses\ParcelChangeContractIDResponse {
        $value = $parcelID ?: $barCode;
        switch (true) {
            case !empty($parcelID):
                $searchMode = 'parcelID';
                break;
            case !empty($barCode):
                $searchMode = 'barCode';
                break;
        }

        if (empty($value) || empty($searchMode)) {
            throw new ValidateRequestException('Ones of parameters are required.');
        }

        return self::factoryEntity(
            self::doRequest(
                HttpClient::PUT,
                self::PARCEL_CHANGE_CONTRACT_ID,
                $request,
                ['query' => [$searchMode => $value]]
            ),
            Responses\ParcelChangeContractIDResponse::class
        );
    }

    public function createParcel(Requests\CreateParcelRequest $request): ?Responses\CreateParcelResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::PARCEL, $request),
            Responses\CreateParcelResponse::class
        );
    }

    public function createParcelGeo(Requests\CreateParcelRequest $request): ?Responses\CreateParcelResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::PARCEL_GEO, $request),
            Responses\CreateParcelResponse::class
        );
    }

    public function updateParcel(
        string $parcelID,
        Requests\UpdateParcelRequest $request
    ): ?Responses\UpdateParcelResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::PARCEL . "/$parcelID", $request),
            Responses\UpdateParcelResponse::class
        );
    }

    public function updateParcelGeo(
        string $parcelID,
        Requests\UpdateParcelRequest $request
    ): ?Responses\UpdateParcelResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::PARCEL_GEO . "/$parcelID", $request),
            Responses\UpdateParcelResponse::class
        );
    }

    public function destroyParcel(string $parcelID): ?Responses\DestroyParcelResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::DELETE, self::PARCEL . "/$parcelID", null),
            Responses\DestroyParcelResponse::class
        );
    }

    public function calculate(Requests\CalculateRequest $request): ?Responses\CalculateResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::CALCULATE, $request),
            Responses\CalculateResponse::class
        );
    }

    public function parcelsList(string $dateFrom): ?Responses\ParcelsListResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCELS_LIST . "/$dateFrom", null),
            Responses\ParcelsListResponse::class
        );
    }

    public function orderDateInfo(string $streetID): ?Responses\OrderDateInfoResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ORDER_DATE_INFO . "/$streetID", null),
            Responses\OrderDateInfoResponse::class
        );
    }

    public function packTypes(): ?Responses\PackTypesResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PACK_TYPES, null),
            Responses\PackTypesResponse::class
        );
    }

    public function specConditions(): ?Responses\SpecConditionsResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::SPEC_CONDITIONS, null),
            Responses\SpecConditionsResponse::class
        );
    }

    public function info4Sticker(string $parcelID): ?Responses\Info4StickerResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::INFO_STICKER . "/$parcelID", null),
            Responses\Info4StickerResponse::class
        );
    }

    public function photoFixation(string $number): ?Responses\PhotoFixationResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PHOTO_FIXATION . "/$number", null),
            Responses\PhotoFixationResponse::class
        );
    }

    // TODO: check response
    public function parcelGoodsReturn(string $parcelID): ?Responses\ParcelGoodsReturnResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PARCEL_GOODS_RETURN . "/$parcelID", null),
            Responses\ParcelGoodsReturnResponse::class
        );
    }

    public function lockParcel(Requests\LockParcelRequest $request): ?Responses\LockParcelResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::LOCK_PARCEL, $request),
            Responses\LockParcelResponse::class
        );
    }

    public function unlockParcel(Requests\UnlockParcelRequest $request): ?Responses\UnlockParcelResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::UNLOCK_PARCEL, $request),
            Responses\UnlockParcelResponse::class
        );
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
