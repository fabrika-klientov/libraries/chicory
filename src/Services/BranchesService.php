<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Branches\Requests;
use Chicory\Entities\Branches as Entities;
use Illuminate\Support\Collection;

class BranchesService extends BaseService
{
    protected const BANNERS = 'branches';
    protected const LOCATOR = 'locator';
    protected const LOCATOR_COORD = 'locator/coord';

    protected $keysCollect = [self::RESULT_CODE];

    public function branches(Requests\BranchesRequest $request = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::BANNERS, $request),
            Entities\BranchSimple::class // can be full data as Branch if filter full
        );
    }

    public function find(int $number): ?Entities\Branch
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::BANNERS . "/$number", null),
            Entities\Branch::class
        )
            ->first();
    }

    public function findByAddress(string $address, string $type = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::LOCATOR . "/$address", ['type' => $type]),
            Entities\Branch::class
        );
    }

    public function findByCoord(string $lat, string $lng, string $type = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::LOCATOR_COORD, ['lat' => $lat, 'lng' => $lng, 'type' => $type]),
            Entities\Branch::class
        );
    }

    protected function useAuth(): bool
    {
        return false;
    }
}
