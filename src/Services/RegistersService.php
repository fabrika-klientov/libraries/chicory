<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Registers\Requests;
use Chicory\Entities\Registers\Responses;

class RegistersService extends BaseService
{
    protected const REGISTER_BRANCH = 'registerBranch';
    protected const REGISTER_PICKUP = 'registerPickup';
    protected const REGISTERS_LIST = 'registersList';

    public function createRegisterBranch(Requests\RegisterBranchRequest $request): ?Responses\RegisterBranchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::REGISTER_BRANCH, $request),
            Responses\RegisterBranchResponse::class
        );
    }

    public function updateRegisterBranch(
        string $registerID,
        Requests\RegisterBranchRequest $request
    ): ?Responses\RegisterBranchResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::REGISTER_BRANCH . "/$registerID", $request),
            Responses\RegisterBranchResponse::class
        );
    }

    public function destroyRegisterBranch(string $registerID): ?Responses\DestroyRegisterBranchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::DELETE, self::REGISTER_BRANCH . "/$registerID", null),
            Responses\DestroyRegisterBranchResponse::class
        );
    }

    public function createRegisterPickup(Requests\RegisterPickupRequest $request): ?Responses\RegisterPickupResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::REGISTER_PICKUP, $request),
            Responses\RegisterPickupResponse::class
        );
    }

    public function updateRegisterPickup(
        string $registerID,
        Requests\RegisterPickupRequest $request
    ): ?Responses\RegisterPickupResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::REGISTER_PICKUP . "/$registerID", $request),
            Responses\RegisterPickupResponse::class
        );
    }

    public function destroyRegisterPickup(string $registerID): ?Responses\DestroyRegisterPickupResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::DELETE, self::REGISTER_PICKUP . "/$registerID", null),
            Responses\DestroyRegisterPickupResponse::class
        );
    }

    public function registersList(string $dateFrom): ?Responses\RegistersListResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::REGISTERS_LIST . "/$dateFrom", null),
            Responses\RegistersListResponse::class
        );
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
