<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Geo\Requests;
use Chicory\Entities\Geo as Entities;
use Illuminate\Support\Collection;

class GeoService extends BaseService
{
    protected const REGIONS = 'geo_regions';
    protected const DISTRICTS = 'geo_districts';
    protected const LOCALITIES = 'geo_localities';
    protected const STREETS = 'geo_streets';

    protected $keysCollect = [self::RESULT_CODE];

    public function regions(Requests\RegionsRequest $request = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::REGIONS, $request),
            Entities\Region::class
        );
    }

    public function districts(Requests\DistrictsRequest $request = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::DISTRICTS, $request),
            Entities\District::class
        );
    }

    public function cities(Requests\CitiesRequest $request = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::LOCALITIES, $request),
            Entities\City::class,
            null,
            ['data']
        );
    }

    public function streets(Requests\StreetsRequest $request): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::STREETS, $request),
            Entities\Street::class
        );
    }

    protected function useAuth(): bool
    {
        return false;
    }
}
