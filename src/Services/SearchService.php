<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Search\Requests;
use Chicory\Entities\Search\Responses;

class SearchService extends BaseService
{
    protected const ADDRESS_BY_COORD = 'getAddressByCoord';
    protected const ADDRESS_SEARCH_BY_COORD = 'addressSearchByCoord';
    protected const COUNTRY_SEARCH = 'countrySearch';
    protected const REGION_SEARCH = 'regionSearch';
    protected const DISTRICT_SEARCH = 'districtSearch';
    protected const CITY_SEARCH = 'citySearch';
    protected const ZIP_CODE_SEARCH = 'zipCodeSearch';
    protected const ADDRESS_SEARCH = 'addressSearch';
    protected const BRANCH_TYPES = 'branchTypes';
    protected const BRANCH_SEARCH = 'branchSearch';
    protected const BRANCH_SEARCH_GEO = 'branchSearchGeo';
    protected const BRANCH_SEARCH_LOCATION = 'branchSearchLocation';
    protected const PAY_TERMINAL_SEARCH = 'payTerminalSearch';

    public function getAddressByCoord(Requests\AddressByCoordRequest $request): ?Responses\AddressByCoordResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::ADDRESS_BY_COORD, $request),
            Responses\AddressByCoordResponse::class
        );
    }

    public function addressSearchByCoord(
        Requests\AddressSearchByCoordRequest $request
    ): ?Responses\AddressSearchByCoordResponse {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::ADDRESS_SEARCH_BY_COORD, $request),
            Responses\AddressSearchByCoordResponse::class
        );
    }

    public function countrySearch(Requests\CountrySearchRequest $request): ?Responses\CountrySearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::COUNTRY_SEARCH, $request),
            Responses\CountrySearchResponse::class
        );
    }

    public function regionSearch(Requests\RegionSearchRequest $request): ?Responses\RegionSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::REGION_SEARCH, $request),
            Responses\RegionSearchResponse::class
        );
    }

    public function districtSearch(Requests\DistrictSearchRequest $request): ?Responses\DistrictSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DISTRICT_SEARCH, $request),
            Responses\DistrictSearchResponse::class
        );
    }

    public function citySearch(Requests\CitySearchRequest $request): ?Responses\CitySearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::CITY_SEARCH, $request),
            Responses\CitySearchResponse::class
        );
    }

    public function zipCodeSearch(string $zipCode): ?Responses\ZipCodeSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ZIP_CODE_SEARCH . "/$zipCode", null),
            Responses\ZipCodeSearchResponse::class
        );
    }

    public function addressSearch(Requests\AddressSearchRequest $request): ?Responses\AddressSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::ADDRESS_SEARCH, $request),
            Responses\AddressSearchResponse::class
        );
    }

    public function branchTypes(): ?Responses\BranchTypesResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::BRANCH_TYPES, null),
            Responses\BranchTypesResponse::class
        );
    }

    public function branchSearch(Requests\BranchSearchRequest $request): ?Responses\BranchSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::BRANCH_SEARCH, $request),
            Responses\BranchSearchResponse::class
        );
    }

    /**
     * @param string|float $latitude
     * @param string|float $longitude
     * @param string|float $radius
     */
    public function branchSearchGeo($latitude, $longitude, $radius): ?Responses\BranchSearchGeoResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::BRANCH_SEARCH_GEO . "/$latitude/$longitude/$radius", null),
            Responses\BranchSearchGeoResponse::class
        );
    }

    /**
     * @param string|float $latitude
     * @param string|float $longitude
     * @param string|float $radius
     */
    public function branchSearchLocation($latitude, $longitude, $radius): ?Responses\BranchSearchLocationResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::BRANCH_SEARCH_LOCATION . "/$latitude/$longitude/$radius", null),
            Responses\BranchSearchLocationResponse::class
        );
    }

    /**
     * @param string|float $latitude
     * @param string|float $longitude
     * @param string|float $radius
     */
    public function payTerminalSearch($latitude, $longitude, $radius): ?Responses\PayTerminalSearchResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PAY_TERMINAL_SEARCH . "/$latitude/$longitude/$radius", null),
            Responses\PayTerminalSearchResponse::class
        );
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
