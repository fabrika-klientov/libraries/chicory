<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Services;

use Chicory\Core\Http\HttpClient;
use Chicory\Entities\Other\Responses;

class OtherService extends BaseService
{
    protected const BANNERS = 'banners';

    public function banners(): ?Responses\BannersResponse
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::BANNERS, null),
            Responses\BannersResponse::class
        );
    }

    protected function useAuth(): bool
    {
        return true;
    }
}
