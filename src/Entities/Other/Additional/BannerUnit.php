<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Other\Additional;

use Chicory\Entities\Other\Entity;

/**
 * @property-read string $UA
 * @property-read string $EN
 * @property-read string $RU
 * */
class BannerUnit extends Entity
{

}
