<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Other;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Other\Additional\BannerUnit;

/**
 * @property-read array $file
 * @property-read array $url
 * */
class Banner extends Entity implements BeEntity
{
    public function file(): ?BannerUnit
    {
        return empty($this->file) ? null : new BannerUnit($this->file);
    }

    public function url(): ?BannerUnit
    {
        return empty($this->url) ? null : new BannerUnit($this->url);
    }
}
