<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Other\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Other\Banner;
use Illuminate\Support\Collection;

class BannersResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Banner[]
     */
    public function result(): array
    {
        return self::getCollectOfData(Banner::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(Banner::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
