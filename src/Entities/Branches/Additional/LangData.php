<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches\Additional;

use Chicory\Entities\Branches\Entity;

/**
 * @property-read string $ua
 * @property-read string $ru
 * @property-read string $en
 * */
class LangData extends Entity
{

}
