<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches\Additional;

use Chicory\Entities\Branches\Entity;

/**
 * @property-read int $receiving_only
 * @property-read int $sending_only
 * @property-read int $format_limit
 * @property-read int $receiving_phone_reqired
 * @property-read int $cash_pay_unavailible
 * @property-read int $cash_terminal_availible
 * @property-read int $place_max_kg
 * @property-read int $parcel_max_kg
 * @property-read int $parcel_max_m3
 * @property-read int $max_insurance
 * @property-read int $max_places
 * @property-read int $max_length
 * @property-read int $max_width
 * @property-read int $max_height
 * @property-read int $free_storage_days
 * */
class Limits extends Entity
{

}
