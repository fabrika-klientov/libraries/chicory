<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches\Additional;

use Chicory\Entities\Branches\Entity;

/**
 * @property-read int $mo
 * @property-read string $mo_s
 * @property-read string $mo_e
 * @property-read string $mo_lb_s
 * @property-read string $mo_lb_e
 *
 * @property-read int $tu
 * @property-read string $tu_s
 * @property-read string $tu_e
 * @property-read string $tu_lb_s
 * @property-read string $tu_lb_e
 *
 * @property-read int $we
 * @property-read string $we_s
 * @property-read string $we_e
 * @property-read string $we_lb_s
 * @property-read string $we_lb_e
 *
 * @property-read int $th
 * @property-read string $th_s
 * @property-read string $th_e
 * @property-read string $th_lb_s
 * @property-read string $th_lb_e
 *
 * @property-read int $fr
 * @property-read string $fr_s
 * @property-read string $fr_e
 * @property-read string $fr_lb_s
 * @property-read string $fr_lb_e
 *
 * @property-read int $sa
 * @property-read string $sa_s
 * @property-read string $sa_e
 * @property-read string $sa_lb_s
 * @property-read string $sa_lb_e
 *
 * @property-read int $su
 * @property-read string $su_s
 * @property-read string $su_e
 * @property-read string $su_lb_s
 * @property-read string $su_lb_e
 * */
class WorkingHoursDetailed extends Entity
{

}
