<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self viewdata(string $value)
 * @method self city(string $value)
 * @method self type(string $value)
 * @method self lang(string $value)
 * */
class BranchesRequest extends BaseRequest implements BeRequestEntity
{
    public const VIEW_DATA_FULL = 'full';

    public const TYPE_POSHTOMAT = 'poshtomat';
    public const TYPE_MINIBRANCH = 'minibranch';
    public const TYPE_MAINBRANCH = 'mainbranch';
    public const TYPE_CARGOBRANCH = 'cargobranch';
    public const TYPE_INTERNATIONALCARGOBRANCH = 'internationalcargobranch';

    public const LANG_UA = 'ua';
    public const LANG_RU = 'ru';
    public const LANG_EN = 'en';
}
