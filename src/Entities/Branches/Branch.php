<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches;

use Chicory\Entities\Branches\Additional\District;
use Chicory\Entities\Branches\Additional\Limits;
use Chicory\Entities\Branches\Additional\Region;
use Chicory\Entities\Branches\Additional\Street;
use Chicory\Entities\Branches\Additional\WorkingHoursDetailed;

/**
 * @property-read string $region_id
 * @property-read array $region
 * @property-read string $district_id
 * @property-read array $district
 * @property-read string $street_id
 * @property-read array $street
 * @property-read string $street_number
 * @property-read string $zip
 * @property-read string $location_description
 * @property-read string $working_hours
 * @property-read array $working_hours_detailed
 * @property-read array $limits
 *
 * @property-read null $parcel_max_kg
 * */
class Branch extends BranchSimple
{
    public function region(): ?Region
    {
        return empty($this->region) ? null : new Region($this->region);
    }

    public function district(): ?District
    {
        return empty($this->district) ? null : new District($this->district);
    }

    public function street(): ?Street
    {
        return empty($this->street) ? null : new Street($this->street);
    }

    public function workingHoursDetailed(): ?WorkingHoursDetailed
    {
        return empty($this->working_hours_detailed) ? null : new WorkingHoursDetailed($this->working_hours_detailed);
    }

    public function limits(): ?Limits
    {
        return empty($this->limits) ? null : new Limits($this->limits);
    }
}
