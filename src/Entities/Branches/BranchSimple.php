<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Branches;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Branches\Additional\City;
use Chicory\Entities\Branches\Additional\TypePublic;

/**
 * @property-read string $br_id
 * @property-read int $num
 * @property-read string $type_id
 * @property-read array $type_public
 * @property-read string $city_id
 * @property-read array $city
 * @property-read string $lng
 * @property-read string $lat
 * @property-read int $parcel_max_kg
 * */
class BranchSimple extends Entity implements BeEntity
{
    public function typePublic(): ?TypePublic
    {
        return empty($this->type_public) ? null : new TypePublic($this->type_public);
    }

    public function city(): ?City
    {
        return empty($this->city) ? null : new City($this->city);
    }
}
