<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking\Additional;

use Chicory\Entities\Tracking\Entity;

/**
 * @property-read string $descrUA
 * @property-read string $descrRU
 * @property-read string $descrEN
 * */
class EventCityDescr extends Entity
{

}
