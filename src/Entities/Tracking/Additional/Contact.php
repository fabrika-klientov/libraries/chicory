<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking\Additional;

use Chicory\Entities\Tracking\Entity;

/**
 * @property-read string $phone
 * @property-read string $cityUA
 * @property-read string $cityRU
 * @property-read string $cityEN
 * @property-read string $countryUA
 * @property-read string $countryRU
 * @property-read string $countryEN
 * @property-read string $service
 * */
class Contact extends Entity
{

}
