<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Tracking\Tracking;
use Illuminate\Support\Collection;

class TrackingResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Tracking[]
     */
    public function result(): array
    {
        return self::getCollectOfData(Tracking::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(Tracking::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
