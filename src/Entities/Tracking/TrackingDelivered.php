<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Tracking\Additional\EventCityDescr;
use Chicory\Entities\Tracking\Additional\EventCountryDescr;

/**
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read string $eventDateTime
 * @property-read array $eventCountryDescr
 * @property-read array $eventCityDescr
 * */
class TrackingDelivered extends Entity implements BeEntity
{
    public function eventCountryDescr(): ?EventCountryDescr
    {
        return empty($this->eventCountryDescr) ? null : new EventCountryDescr($this->eventCountryDescr);
    }

    public function eventCityDescr(): ?EventCityDescr
    {
        return empty($this->eventCityDescr) ? null : new EventCityDescr($this->eventCityDescr);
    }
}
