<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Tracking\Additional\EventCityDescr;
use Chicory\Entities\Tracking\Additional\EventCountryDescr;
use Chicory\Entities\Tracking\Additional\EventDescr;
use Chicory\Entities\Tracking\Additional\EventDetailDescr;

/**
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read string $eventDateTime
 * @property-read string $eventCode
 * @property-read string $eventCodeUPU
 * @property-read array $eventCountryDescr
 * @property-read array $eventCityDescr
 * @property-read array $eventDescr
 * @property-read array $eventDetailDescr
 * */
class Tracking extends Entity implements BeEntity
{
    public function eventCountryDescr(): ?EventCountryDescr
    {
        return empty($this->eventCountryDescr) ? null : new EventCountryDescr($this->eventCountryDescr);
    }

    public function eventCityDescr(): ?EventCityDescr
    {
        return empty($this->eventCityDescr) ? null : new EventCityDescr($this->eventCityDescr);
    }

    public function eventDescr(): ?EventDescr
    {
        return empty($this->eventDescr) ? null : new EventDescr($this->eventDescr);
    }

    public function eventDetailDescr(): ?EventDetailDescr
    {
        return empty($this->eventDetailDescr) ? null : new EventDetailDescr($this->eventDetailDescr);
    }
}
