<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Tracking;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Tracking\Additional\Contact;

/**
 * @property-read string $parcelID
 * @property-read string $parcelIDref
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read string $PDD
 * @property-read string $JIT0
 * @property-read array $sender
 * @property-read array $receiver
 * @property-read string $plannedDeliveryDate
 * */
class ParcelInfoTracking extends Entity implements BeEntity
{
    public function sender(): ?Contact
    {
        return empty($this->sender) ? null : new Contact($this->sender);
    }

    public function receiver(): ?Contact
    {
        return empty($this->receiver) ? null : new Contact($this->receiver);
    }
}
