<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Geo;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $ua
 * @property-read string $ru
 * @property-read string $en
 * @property-read string $t_ua
 * @property-read string $t_ru
 * @property-read string $t_en
 * @property-read string $street_id
 * @property-read string $city_id
 * @property-read string $kt
 * */
class Street extends Entity implements BeEntity
{

}
