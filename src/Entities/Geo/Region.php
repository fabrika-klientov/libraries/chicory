<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Geo;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $ua
 * @property-read string $ru
 * @property-read string $en
 * @property-read string $region_id
 * @property-read string $kt
 * */
class Region extends Entity implements BeEntity
{

}
