<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Geo\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self searchFull(string $value)
 * @method self searchBeginning(string $value)
 * */
class RegionsRequest extends BaseRequest implements BeRequestEntity
{

}
