<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities;

use Chicory\Core\Classes\ActiveRecord;

abstract class BaseRequest extends ActiveRecord
{
    public function getData(): array
    {
        return $this->data;
    }

    protected function with(string $key, $value)
    {
        $this->setData($key, $value);

        return $this;
    }

    protected function setData(string $key, $value)
    {
        $this->data[$key] = $value;
    }

    protected function remove(string $key)
    {
        unset($this->data[$key]);
    }

    protected function filters(string $key, $value)
    {
        if (empty($this->data['filters'])) {
            $this->data['filters'] = [];
        }

        $this->data['filters'][$key] = $value;

        return $this;
    }

    public function __call($name, $arguments)
    {
        return $this->with($name, $arguments[0]);
    }

    /**
     * @throws \Chicory\Exceptions\ValidateRequestException
     */
    public function validate(): bool
    {
        return true;
    }
}
