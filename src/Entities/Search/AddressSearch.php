<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\AddressDescr;
use Chicory\Entities\Search\Additional\CityDescr;

/**
 * @property-read string $addressID
 * @property-read array $addressDescr
 * @property-read string $cityID
 * @property-read array $cityDescr
 * @property-read string $Localization
 * */
class AddressSearch extends Entity implements BeEntity
{
    public function addressDescr(): ?AddressDescr
    {
        return empty($this->addressDescr) ? null : new AddressDescr($this->addressDescr);
    }

    public function cityDescr(): ?CityDescr
    {
        return empty($this->cityDescr) ? null : new CityDescr($this->cityDescr);
    }
}
