<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Search\RegionSearch;
use Illuminate\Support\Collection;

class RegionSearchResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return RegionSearch[]
     */
    public function result(): array
    {
        return self::getCollectOfData(RegionSearch::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(RegionSearch::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
