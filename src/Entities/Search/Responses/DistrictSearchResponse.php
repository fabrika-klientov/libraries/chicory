<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2022 Fabrika-Klientov
 * @version   GIT: 22.01.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Search\DistrictSearch;
use Illuminate\Support\Collection;

class DistrictSearchResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return DistrictSearch[]
     */
    public function result(): array
    {
        return self::getCollectOfData(DistrictSearch::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(DistrictSearch::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
