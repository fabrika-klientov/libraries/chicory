<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Search\AddressSearchByCoord;

class AddressSearchByCoordResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return AddressSearchByCoord
     *
     * @throws \Chicory\Exceptions\ChicoryException
     */
    public function result(): AddressSearchByCoord
    {
        return self::factoryObjectWithThrow(AddressSearchByCoord::class);
    }
}
