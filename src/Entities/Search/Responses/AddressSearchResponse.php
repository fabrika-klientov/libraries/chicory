<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Search\AddressSearch;
use Illuminate\Support\Collection;

class AddressSearchResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return AddressSearch[]
     */
    public function result(): array
    {
        return self::getCollectOfData(AddressSearch::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(AddressSearch::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
