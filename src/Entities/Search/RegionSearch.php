<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\CountryDescr;
use Chicory\Entities\Search\Additional\RegionDescr;

/**
 * @property-read string $regionID
 * @property-read string $regionKATUU
 * @property-read array $regionDescr
 * @property-read string $countryID
 * @property-read array $descrCountry
 * @property-read string $Localization
 * */
class RegionSearch extends Entity implements BeEntity
{
    public function regionDescr(): ?RegionDescr
    {
        return empty($this->regionDescr) ? null : new RegionDescr($this->regionDescr);
    }

    public function descrCountry(): ?CountryDescr
    {
        return empty($this->descrCountry) ? null : new CountryDescr($this->descrCountry);
    }
}
