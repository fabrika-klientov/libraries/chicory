<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\AddressDescr;
use Chicory\Entities\Search\Additional\BranchDescr;
use Chicory\Entities\Search\Additional\BranchLimits;
use Chicory\Entities\Search\Additional\BranchWorkTime;
use Chicory\Entities\Search\Additional\CityDescr;
use Chicory\Entities\Search\Additional\DistrictDescr;
use Chicory\Entities\Search\Additional\RegionDescr;

/**
 * @property-read string $branchID
 * @property-read string $branchIDref
 * @property-read int $branchNo
 * @property-read string $branchType
 * @property-read string $branchTypeID
 * @property-read string $branchTypeDescr
 * @property-read int $branchTypeAPP
 * @property-read int $networkDepartment
 * @property-read string $networkPartner
 * @property-read string $networkPartnerCode
 * @property-read array $branchDescr
 * @property-read string $addressID
 * @property-read array $addressDescr
 * @property-read string $addressMoreInformation
 * @property-read string $cityID
 * @property-read array $cityDescr
 * @property-read string $districtID
 * @property-read array $districtDescr
 * @property-read string $regionID
 * @property-read array $regionDescr
 * @property-read string $workingHours
 * @property-read string $building
 * @property-read string $zipCode
 * @property-read string $latitude
 * @property-read string $longitude
 * @property-read array $branchWorkTime
 * @property-read string $phone
 * @property-read string $address
 * @property-read string $paymentTypes
 * @property-read array $branchLimits
 * @property-read string $Localization
 * */
class BranchSearch extends Entity implements BeEntity
{
    public function branchDescr(): ?BranchDescr
    {
        return empty($this->branchDescr) ? null : new BranchDescr($this->branchDescr);
    }

    public function addressDescr(): ?AddressDescr
    {
        return empty($this->addressDescr) ? null : new AddressDescr($this->addressDescr);
    }

    public function cityDescr(): ?CityDescr
    {
        return empty($this->cityDescr) ? null : new CityDescr($this->cityDescr);
    }

    public function districtDescr(): ?DistrictDescr
    {
        return empty($this->districtDescr) ? null : new DistrictDescr($this->districtDescr);
    }

    public function regionDescr(): ?RegionDescr
    {
        return empty($this->regionDescr) ? null : new RegionDescr($this->regionDescr);
    }

    /**
     * @return BranchWorkTime[]
     * */
    public function branchWorkTime(): array
    {
        return self::collectFor('branchWorkTime', BranchWorkTime::class);
    }

    public function branchLimits(): ?BranchLimits
    {
        return empty($this->branchLimits) ? null : new BranchLimits($this->branchLimits);
    }
}
