<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\Address;
use Chicory\Entities\Search\Additional\GeoPoint;

/**
 * @property-read array $geoPoint
 * @property-read array $address
 * */
class AddressByCoord extends Entity implements BeEntity
{
    public function geoPoint(): ?GeoPoint
    {
        return empty($this->geoPoint) ? null : new GeoPoint($this->geoPoint);
    }

    public function address(): ?Address
    {
        return empty($this->address) ? null : new Address($this->address);
    }
}
