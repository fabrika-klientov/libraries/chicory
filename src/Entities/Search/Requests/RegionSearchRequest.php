<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self regionID(string $value)
 * @method self regionKATUU(string $value)
 * @method self regionDescr(string $value)
 * @method self countryID(string $value)
 * @method self countryDescr(string $value)
 * */
class RegionSearchRequest extends BaseFiltersRequest implements BeRequestEntity
{

}
