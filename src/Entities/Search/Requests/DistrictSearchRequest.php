<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2022 Fabrika-Klientov
 * @version   GIT: 22.01.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self districtID(string $value)
 * @method self districtDescr(string $value)
 * @method self regionID(string $value)
 * @method self regionDescr(string $value)
 * @method self districtKATUU(string $value)
 * */
class DistrictSearchRequest extends BaseFiltersRequest implements BeRequestEntity
{

}
