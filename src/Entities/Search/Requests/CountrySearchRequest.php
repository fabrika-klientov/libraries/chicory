<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self countryID(string $value)
 * @method self countryDescr(string $value)
 * */
class CountrySearchRequest extends BaseFiltersRequest implements BeRequestEntity
{

}
