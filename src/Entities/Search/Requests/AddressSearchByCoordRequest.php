<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self lat($value)
 * @method self lon($value)
 * */
class AddressSearchByCoordRequest extends BaseRequest implements BeRequestEntity
{

}
