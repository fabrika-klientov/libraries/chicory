<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self branchNo(int $value)
 * @method self branchTypeID(string $value)
 * @method self branchDescr(string $value)
 * @method self cityID(string $value)
 * @method self cityDescr(string $value)
 * @method self districtID(string $value)
 * @method self districtDescr(string $value)
 * @method self regionID(string $value)
 * @method self regionDescr(string $value)
 * @method self networkDepartment(bool $value)
 * @method self networkPartner(string $value)
 * @method self networkPartnerCode(string $value)
 * @method self addressMoreInformation(string $value)
 * */
class BranchSearchRequest extends BaseFiltersRequest implements BeRequestEntity
{
    public function getCobranding(bool $value)
    {
        return $this->with('getCobranding', $value);
    }

    public function cobrandingCode($value)
    {
        return $this->with('cobrandingCode', $value);
    }

    public function in(bool $value)
    {
        return $this->with('in', $value);
    }

    public function out(bool $value)
    {
        return $this->with('out', $value);
    }

    public function test(bool $value)
    {
        return $this->with('test', $value);
    }

    public function closed(bool $value)
    {
        return $this->with('closed', $value);
    }
}
