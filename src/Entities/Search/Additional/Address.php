<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Additional;

use Chicory\Entities\Search\Entity;

/**
 * @property-read string $city
 * @property-read string $country
 * @property-read string $region
 * @property-read string $postcode
 * @property-read string $county
 *
 * @property-read string $borough // for SearchService::getAddressByCoord
 * @property-read string $street // for SearchService::getAddressByCoord
 * @property-read string $house_number // for SearchService::getAddressByCoord
 * */
class Address extends Entity
{

}
