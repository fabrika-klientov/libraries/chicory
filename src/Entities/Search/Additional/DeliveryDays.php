<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Additional;

use Chicory\Entities\Search\Entity;

/**
 * @property-read bool $Mon
 * @property-read bool $Tue
 * @property-read bool $Wed
 * @property-read bool $Thu
 * @property-read bool $Fri
 * @property-read bool $Sat
 * @property-read bool $Sun
 * */
class DeliveryDays extends Entity
{

}
