<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Additional;

use Chicory\Entities\Search\Entity;

/**
 * @property-read int $length
 * @property-read int $width
 * @property-read int $height
 *
 * @property-read int $weightMax // for detail of Branch
 * */
class BranchLimitsGabaritesMax extends Entity
{

}
