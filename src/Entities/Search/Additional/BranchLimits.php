<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Additional;

use Chicory\Entities\Search\Entity;

/**
 * @property-read float $weightTotalMax
 * @property-read float $weightPlaceMax
 * @property-read float $volumeTotalMax
 * @property-read float $quantityPlacesMax
 * @property-read float $insuranceTotalMax
 * @property-read int $storageDays
 * @property-read array $gabaritesMax
 *
 * @property-read bool $formatLimit
 * @property-read bool $cashPayUnavailible
 * @property-read bool $sendingOnly
 * @property-read bool $receivingOnly
 * @property-read bool $receiverCellPhoneRequired
 * @property-read bool $terminalCash
 * */
class BranchLimits extends Entity
{
    public function gabaritesMax(): ?BranchLimitsGabaritesMax
    {
        return empty($this->gabaritesMax) ? null : new BranchLimitsGabaritesMax($this->gabaritesMax);
    }
}
