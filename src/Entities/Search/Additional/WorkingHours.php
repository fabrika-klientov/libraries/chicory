<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search\Additional;

use Chicory\Entities\Search\Entity;

/**
 * @property-read string $descrUA
 * @property-read string $descrRU
 * */
class WorkingHours extends Entity
{

}
