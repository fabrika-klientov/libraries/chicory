<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\AddressDescr;
use Chicory\Entities\Search\Additional\AddressMoreInformationDescr;
use Chicory\Entities\Search\Additional\CityDescr;
use Chicory\Entities\Search\Additional\PayTerminalDescr;
use Chicory\Entities\Search\Additional\WorkingHours;

/**
 * @property-read string $type
 * @property-read string $longitude
 * @property-read string $latitude
 * @property-read array $payTerminalDescr
 * @property-read array $workingHours
 * @property-read array $cityDescr
 * @property-read array $addressDescr
 * @property-read array $addressMoreInformationDescr
 * */
class PayTerminalSearch extends Entity implements BeEntity
{
    public function payTerminalDescr(): ?PayTerminalDescr
    {
        return empty($this->payTerminalDescr) ? null : new PayTerminalDescr($this->payTerminalDescr);
    }

    public function workingHours(): ?WorkingHours
    {
        return empty($this->workingHours) ? null : new WorkingHours($this->workingHours);
    }

    public function cityDescr(): ?CityDescr
    {
        return empty($this->cityDescr) ? null : new CityDescr($this->cityDescr);
    }

    public function addressDescr(): ?AddressDescr
    {
        return empty($this->addressDescr) ? null : new AddressDescr($this->addressDescr);
    }

    public function addressMoreInformationDescr(): ?AddressMoreInformationDescr
    {
        return empty($this->addressMoreInformationDescr)
            ? null : new AddressMoreInformationDescr($this->addressMoreInformationDescr);
    }
}
