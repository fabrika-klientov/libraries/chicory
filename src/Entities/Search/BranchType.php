<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\BranchLimits;
use Chicory\Entities\Search\Additional\BranchTypeDescr;

/**
 * @property-read string $branchTypeID
 * @property-read array $branchTypeDescr
 * @property-read array $branchLimits
 * */
class BranchType extends Entity implements BeEntity
{
    public function branchTypeDescr(): ?BranchTypeDescr
    {
        return empty($this->branchTypeDescr) ? null : new BranchTypeDescr($this->branchTypeDescr);
    }

    public function branchLimits(): ?BranchLimits
    {
        return empty($this->branchLimits) ? null : new BranchLimits($this->branchLimits);
    }
}
