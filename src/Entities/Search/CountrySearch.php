<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\CountryDescr;

/**
 * @property-read string $countryID
 * @property-read array $countryDescr
 * @property-read string $alfaCode2
 * @property-read string $Localization
 * */
class CountrySearch extends Entity implements BeEntity
{
    public function countryDescr(): ?CountryDescr
    {
        return empty($this->countryDescr) ? null : new CountryDescr($this->countryDescr);
    }
}
