<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\CityDescr;
use Chicory\Entities\Search\Additional\DeliveryDays;
use Chicory\Entities\Search\Additional\DistrictDescr;
use Chicory\Entities\Search\Additional\RegionDescr;

/**
 * @property-read string $cityID
 * @property-read string $cityKATUU
 * @property-read array $cityDescr
 * @property-read string $districtID
 * @property-read array $districtDescr
 * @property-read string $regionID
 * @property-read array $regionDescr
 * @property-read string $countryID
 * @property-read bool $isBranchInCity
 * @property-read int $deliveryZone
 * @property-read array $deliveryDays
 * @property-read string $latitude
 * @property-read string $longitude
 * @property-read string $Localization
 * */
class CitySearch extends Entity implements BeEntity
{
    public function cityDescr(): ?CityDescr
    {
        return empty($this->cityDescr) ? null : new CityDescr($this->cityDescr);
    }

    public function regionDescr(): ?RegionDescr
    {
        return empty($this->regionDescr) ? null : new RegionDescr($this->regionDescr);
    }

    public function districtDescr(): ?DistrictDescr
    {
        return empty($this->districtDescr) ? null : new DistrictDescr($this->districtDescr);
    }

    public function deliveryDays(): ?DeliveryDays
    {
        return empty($this->deliveryDays) ? null : new DeliveryDays($this->deliveryDays);
    }
}
