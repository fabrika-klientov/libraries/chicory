<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2022 Fabrika-Klientov
 * @version   GIT: 22.01.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Search;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Search\Additional\DistrictDescr;
use Chicory\Entities\Search\Additional\RegionDescr;

/**
 * @property-read string $districtID
 * @property-read string $districtKATUU
 * @property-read array $districtDescr
 * @property-read string $regionID
 * @property-read array $regionDescr
 * */
class DistrictSearch extends Entity implements BeEntity
{
    public function regionDescr(): ?RegionDescr
    {
        return empty($this->regionDescr) ? null : new RegionDescr($this->regionDescr);
    }

    public function districtDescr(): ?DistrictDescr
    {
        return empty($this->districtDescr) ? null : new DistrictDescr($this->districtDescr);
    }
}
