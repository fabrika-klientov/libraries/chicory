<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities;

use Chicory\Core\Classes\ActiveRecord;
use Illuminate\Support\Collection;

abstract class Entity extends ActiveRecord
{
    /**
     * @param string $key
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     */
    protected function collectFor(string $key, string $className, bool $isCollection = false)
    {
        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $this->{$key} ?? []);

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }
}
