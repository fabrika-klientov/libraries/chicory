<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Registers\RegisterPickup;

class RegisterPickupResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return RegisterPickup
     *
     * @throws \Chicory\Exceptions\ChicoryException
     */
    public function result(): RegisterPickup
    {
        return self::factoryObjectWithThrow(RegisterPickup::class);
    }
}
