<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Registers\RegistersItem;
use Illuminate\Support\Collection;

class RegistersListResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return RegistersItem[]
     */
    public function result(): array
    {
        return self::getCollectOfData(RegistersItem::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(RegistersItem::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
