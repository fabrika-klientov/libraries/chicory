<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Registers\Additional\ParcelsItem;

/**
 * @property-read string $registerID
 * @property-read string $registerNumber
 * @property-read string $dateTime
 * @property-read string $registerType
 * @property-read string $parcelQty
 * @property-read array $parcelsItems
 * */
class RegistersItem extends Entity implements BeEntity
{
    /**
     * @return ParcelsItem[]
     */
    public function parcelsItems(): array
    {
        return self::collectFor('parcelsItems', ParcelsItem::class);
    }
}
