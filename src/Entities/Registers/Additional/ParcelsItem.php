<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Additional;

use Chicory\Entities\Registers\Entity;

/**
 * @property-read string $parcelID
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * */
class ParcelsItem extends Entity
{

}
