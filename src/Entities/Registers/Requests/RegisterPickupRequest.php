<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self notation(string $value)
 * @method self contractID(string $value)
 * @method self payType(string $value)
 * @method self receiverPay(bool $value)
 * @method self expectedPickUpDate(\Chicory\Entities\Registers\Requests\Deep\ExpectedPickUpDate $value)
 * @method self sender(\Chicory\Entities\Registers\Requests\Deep\Sender $value)
 * @method self parcelsItems(\Chicory\Entities\Registers\Requests\Deep\ParcelsItem[] $value)
 * */
class RegisterPickupRequest extends BaseRequest implements BeRequestEntity
{

}
