<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Requests\Deep;

use Chicory\Entities\Registers\Requests\BaseRequest;

/**
 * @method self parcelID(string $value)
 * */
class ParcelsItem extends BaseRequest
{

}
