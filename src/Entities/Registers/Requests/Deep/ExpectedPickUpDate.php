<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers\Requests\Deep;

use Chicory\Entities\Registers\Requests\BaseRequest;

/**
 * @method self date(string $value)
 * @method self timeFrom(string $value)
 * @method self timeTo(string $value)
 * */
class ExpectedPickUpDate extends BaseRequest
{

}
