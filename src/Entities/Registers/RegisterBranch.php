<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Registers;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $registerID
 * @property-read string $registerNumber
 * */
class RegisterBranch extends Entity implements BeEntity
{

}
