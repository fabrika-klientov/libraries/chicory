<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities;

use Chicory\Core\Classes\ActiveRecord;
use Chicory\Entities\Base\Info;
use Chicory\Exceptions\ChicoryException;
use Illuminate\Support\Collection;

/**
 * @property-read string $status
 * @property-read array $info
 * @property-read array $result
 * */
abstract class BaseResponse extends ActiveRecord
{
    /**
     * @return Info
     */
    public function info(): Info
    {
        return new Info($this->info ?? []);
    }

    /**
     * @return mixed
     */
    abstract public function result();

    /**
     * @throws ChicoryException
     */
    protected function factoryObjectWithThrow(string $className)
    {
        if (!class_exists($className)) {
            throw new ChicoryException("Class name [$className] is not resolved");
        }

        if (empty($this->result)) {
            throw new ChicoryException('Data were returned as empty');
        }

        return new $className($this->result);
    }

    /**
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws ChicoryException
     */
    protected function getCollectOfData(string $className, bool $isCollection = false)
    {
        if (!class_exists($className)) {
            throw new ChicoryException("Class name [$className] is not resolved");
        }

        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $this->result ?: []);

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }
}
