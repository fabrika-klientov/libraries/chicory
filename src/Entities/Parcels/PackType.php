<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\PackLimits;

/**
 * @property-read string $packID
 * @property-read string $packDescr
 * @property-read array $packLimits
 * */
class PackType extends Entity implements BeEntity
{
    public function packLimits(): ?PackLimits
    {
        return empty($this->packLimits) ? null : new PackLimits($this->packLimits);
    }
}
