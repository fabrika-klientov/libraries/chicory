<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\Merchant;

/**
 * @property-read string $total
 * @property-read string $COD
 * @property-read float $costServices
 * @property-read bool $directReturn
 * @property-read array $Merchants
 * */
class ParcelDebtInfo extends Entity implements BeEntity
{
    /**
     * @return Merchant[]
     */
    public function merchants(): array
    {
        return self::collectFor('Merchants', Merchant::class);
    }
}
