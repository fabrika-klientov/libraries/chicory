<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\EasyReturnAgentInfoReceiver;

/**
 * @property-read string $contractID
 * @property-read string $payType
 * @property-read bool $receiverPay
 * @property-read array $receiver
 * */
class EasyReturnAgentInfo extends Entity implements BeEntity
{
    /**
     * @return EasyReturnAgentInfoReceiver[]
     * */
    public function receiver(): array
    {
        return self::collectFor('receiver', EasyReturnAgentInfoReceiver::class);
    }
}
