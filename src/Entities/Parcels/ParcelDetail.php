<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\CardForCOD;
use Chicory\Entities\Parcels\Additional\CodPaymentsItem;
use Chicory\Entities\Parcels\Additional\ContentsItem;
use Chicory\Entities\Parcels\Additional\DeliveryOptions;
use Chicory\Entities\Parcels\Additional\Good;
use Chicory\Entities\Parcels\Additional\PlacesItem;
use Chicory\Entities\Parcels\Additional\Receiver;
use Chicory\Entities\Parcels\Additional\Sender;
use Chicory\Entities\Parcels\Additional\SpecConditionsItem;

/**
 * @property-read string $parcelID
 * @property-read string $sendingDate
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read string $subAgent
 * @property-read string $agent
 * @property-read string $notation
 * @property-read string $payType
 * @property-read string $receiverPay
 * @property-read float $costServices
 * @property-read string $COD
 * @property-read string $weight
 * @property-read string $pudoID
 * @property-read string $pudoPointType
 * @property-read array $expectedDeliveryDate
 * @property-read array $sender
 * @property-read array $receiver
 * @property-read array $placesItems
 * @property-read array $specConditionsItems
 * @property-read array $contentsItems
 * @property-read array $codPaymentsItems
 * @property-read array $goods
 * @property-read array $deliveryOptions
 * @property-read array $cardForCOD
 *
 * // additional (ex.: printCN23)
 * @property-read string $agentDescr
 * @property-read string $currency
 * @property-read string $agentCode
 * @property-read string $value
 * @property-read string $container
 * @property-read string $containerDate
 * @property-read string $IMPC
 * */
class ParcelDetail extends Entity implements BeEntity
{
    public function sender(): ?Sender
    {
        return empty($this->sender) ? null : new Sender($this->sender);
    }

    public function receiver(): ?Receiver
    {
        return empty($this->receiver) ? null : new Receiver($this->receiver);
    }

    /**
     * @return PlacesItem[]
     */
    public function placesItems(): array
    {
        return self::collectFor('placesItems', PlacesItem::class);
    }

    /**
     * @return SpecConditionsItem[]
     */
    public function specConditionsItems(): array
    {
        return self::collectFor('specConditionsItems', SpecConditionsItem::class);
    }

    /**
     * @return ContentsItem[]
     */
    public function contentsItems(): array
    {
        return self::collectFor('contentsItems', ContentsItem::class);
    }

    /**
     * @return CodPaymentsItem[]
     */
    public function codPaymentsItems(): array
    {
        return self::collectFor('codPaymentsItems', CodPaymentsItem::class);
    }

    /**
     * @return Good[]
     */
    public function goods(): array
    {
        return self::collectFor('goods', Good::class);
    }

    public function deliveryOptions(): ?DeliveryOptions
    {
        return empty($this->deliveryOptions) ? null : new DeliveryOptions($this->deliveryOptions);
    }

    public function cardForCOD(): ?CardForCOD
    {
        return empty($this->cardForCOD) ? null : new CardForCOD($this->cardForCOD);
    }
}
