<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $parcelID
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read string $branchName
 * @property-read string $branchCode
 * @property-read string $deliveryType
 * @property-read string $formatType
 * @property-read string $totalPlaces
 * @property-read string $hikingServiceRoute
 * @property-read string $mainServiceRoute
 * @property-read string $receiverName
 * @property-read string $receiverPhone
 * @property-read string $receiverCity
 * @property-read string $receiverDistrict
 * @property-read string $receiverRegion
 * @property-read string $receiverAddress
 * @property-read string $receiverBuilding
 * @property-read string $receiverFlat
 * @property-read string $COD
 * @property-read string $notation
 * @property-read string $value
 * */
class Info4Sticker extends Entity implements BeEntity
{

}
