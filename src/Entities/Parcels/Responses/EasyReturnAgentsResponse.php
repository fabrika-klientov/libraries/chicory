<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Parcels\EasyReturnAgent;
use Illuminate\Support\Collection;

class EasyReturnAgentsResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return EasyReturnAgent[]
     */
    public function result(): array
    {
        return self::getCollectOfData(EasyReturnAgent::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(EasyReturnAgent::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
