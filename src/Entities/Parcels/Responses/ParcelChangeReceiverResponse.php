<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Parcels\ParcelChangeReceiver;

class ParcelChangeReceiverResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return ParcelChangeReceiver[]
     *
     * @throws \Chicory\Exceptions\ChicoryException
     */
    public function result(): array
    {
        return self::getCollectOfData(ParcelChangeReceiver::class);
    }
}
