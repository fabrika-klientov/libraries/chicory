<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Parcels\PhotoFixation;
use Illuminate\Support\Collection;

class PhotoFixationResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return PhotoFixation[]
     */
    public function result(): array
    {
        return self::getCollectOfData(PhotoFixation::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(PhotoFixation::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
