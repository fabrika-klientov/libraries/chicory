<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Parcels\ParcelStatus;
use Illuminate\Support\Collection;

class ParcelStatusResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return ParcelStatus[]
     */
    public function result(): array
    {
        return self::getCollectOfData(ParcelStatus::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(ParcelStatus::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
