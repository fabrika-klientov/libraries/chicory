<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Responses;

use Chicory\Contracts\BeResponseEntity;
use Chicory\Entities\Parcels\ParcelOfList;
use Illuminate\Support\Collection;

class ParcelsListResponse extends BaseResponse implements BeResponseEntity
{
    /**
     * @return ParcelOfList[]
     */
    public function result(): array
    {
        return self::getCollectOfData(ParcelOfList::class);
    }

    public function resultCollect(): Collection
    {
        return self::getCollectOfData(ParcelOfList::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->resultCollect()->isEmpty();
    }
}
