<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\ParcelStatusDescr;

/**
 * @property-read string $parcelID
 * @property-read string $parcelNumber
 * @property-read string $barCode
 * @property-read array $parcelStatusDescr
 * @property-read string $registerID
 * @property-read string $registerNumber
 * @property-read string $registerType
 * */
class ParcelOfList extends Entity implements BeEntity
{
    public function parcelStatusDescr(): ?ParcelStatusDescr
    {
        return empty($this->parcelStatusDescr) ? null : new ParcelStatusDescr($this->parcelStatusDescr);
    }
}
