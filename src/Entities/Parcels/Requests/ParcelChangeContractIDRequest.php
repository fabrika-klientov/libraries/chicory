<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self contractID(string $value)
 * @method self payType(string $value)
 * @method self receiverPay(string $value)
 * */
class ParcelChangeContractIDRequest extends BaseRequest implements BeRequestEntity
{

}
