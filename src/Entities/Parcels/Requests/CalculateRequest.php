<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self contractID(string $value)
 * @method self COD(float $value)
 * @method self sendingDate(string $value)
 * @method self sender(\Chicory\Entities\Parcels\Requests\Deep\CalcSender $value)
 * @method self receiver(\Chicory\Entities\Parcels\Requests\Deep\CalcReceiver $value)
 * @method self placesItems(\Chicory\Entities\Parcels\Requests\Deep\PlacesItem[] $value)
 * @method self specConditionsItems(\Chicory\Entities\Parcels\Requests\Deep\SpecConditionsItem[] $value)
 * */
class CalculateRequest extends BaseRequest implements BeRequestEntity
{

}
