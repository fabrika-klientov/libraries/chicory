<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self parcelID(string $value)
 * @method self newReceiver(string $value)
 * @method self newPhone(string $value)
 * */
class ParcelChangeReceiverRequest extends BaseRequest implements BeRequestEntity
{

}
