<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self parcelNumber(string $value)
 * @method self subAgent(string $value)
 * @method self notation(string $value)
 * @method self contractID(string $value)
 * @method self payType(string $value)
 * @method self receiverPay(bool $value)
 * @method self COD(float $value)
 * @method self weight(float $value)
 * @method self sendingDate(string $value)
 * @method self info4Sticker(bool $value)
 * @method self pudoID(string $value)
 * @method self pudoPointType(string $value)
 * @method self pudoDescription(string $value)
 * @method self AgentID(string $value)
 * @method self orderNumber(string $value)
 * @method self barcode(string $value)
 * @method self barcode_myMeest(string $value)
 * @method self bagNumber(string $value)
 * @method self awbNumber(string $value)
 * @method self delivery_payment_payer(string $value)
 * @method self expectedPickUpDate(\Chicory\Entities\Parcels\Requests\Deep\ExpectedPickUpDate $value)
 * @method self expectedDeliveryDate(\Chicory\Entities\Parcels\Requests\Deep\ExpectedDeliveryDate $value)
 * @method self sender(\Chicory\Entities\Parcels\Requests\Deep\Sender $value)
 * @method self receiver(\Chicory\Entities\Parcels\Requests\Deep\Receiver $value)
 * @method self placesItems(\Chicory\Entities\Parcels\Requests\Deep\PlacesItem[] $value)
 * @method self specConditionsItems(\Chicory\Entities\Parcels\Requests\Deep\SpecConditionsItem[] $value)
 * @method self contentsItems(\Chicory\Entities\Parcels\Requests\Deep\ContentsItem[] $value)
 * @method self codPaymentsItems(\Chicory\Entities\Parcels\Requests\Deep\CodPaymentsItem[] $value)
 * @method self goods(\Chicory\Entities\Parcels\Requests\Deep\Good[] $value)
 * @method self deliveryOptions(\Chicory\Entities\Parcels\Requests\Deep\DeliveryOptions $value)
 * @method self cardForCOD(\Chicory\Entities\Parcels\Requests\Deep\CardForCOD $value)
 * */
class CreateParcelRequest extends BaseRequest implements BeRequestEntity
{
    public const PAY_TYPE_CASH = 'cash';
    public const PAY_TYPE_NONCASH = 'noncash';

    public const SERVICE_DOOR = 'Door';
    public const SERVICE_BRANCH = 'Branch';
}
