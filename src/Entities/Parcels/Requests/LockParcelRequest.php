<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests;

use Chicory\Contracts\BeRequestEntity;

/**
 * @method self ClientUID(string $value)
 * @method self ParcelUID(string $value)
 * @method self ClientsShipmentRef(string $value)
 * @method self AppName(string $value)
 * @method self Notation(string $value)
 * */
class LockParcelRequest extends BaseRequest implements BeRequestEntity
{

}
