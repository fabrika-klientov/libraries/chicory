<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests\Deep;

use Chicory\Entities\Parcels\Requests\BaseRequest;

/**
 * @method self quantity(int $value)
 * @method self weight(float $value)
 * @method self volume(float $value)
 * @method self insurance(float $value)
 * @method self packID(string $value)
 * @method self length(int $value)
 * @method self width(int $value)
 * @method self height(int $value)
 * @method self wheels(int $value)
 * @method self size(string $value)
 * */
class PlacesItem extends BaseRequest
{

}
