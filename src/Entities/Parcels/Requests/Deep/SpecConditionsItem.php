<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests\Deep;

use Chicory\Entities\Parcels\Requests\BaseRequest;

/**
 * @method self conditionID(string $value)
 * */
class SpecConditionsItem extends BaseRequest
{

}
