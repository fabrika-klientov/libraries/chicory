<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests\Deep;

use Chicory\Entities\Parcels\Requests\BaseRequest;

/**
 * @method self contentName(string $value)
 * @method self quantity(int $value)
 * @method self weight(float $value)
 * @method self value(float $value)
 * @method self customsCode(string $value)
 * @method self country(string $value)
 * @method self size(string $value)
 * @method self ean13(string $value)
 * @method self gender(string $value)
 * @method self valueReceiverCurrency(string $value)
 * */
class ContentsItem extends BaseRequest
{

}
