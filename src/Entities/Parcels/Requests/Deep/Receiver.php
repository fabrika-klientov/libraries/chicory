<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests\Deep;

use Chicory\Entities\Parcels\Requests\BaseRequest;

/**
 * @method self name(string $value)
 * @method self phone(string $value)
 * @method self zipCode(string $value)
 * @method self branchID(string $value)
 * @method self addressID(string $value)
 * @method self building(string $value)
 * @method self flat(string $value)
 * @method self floor(int $value)
 * @method self service(string $value)
 * @method self corps(string $value)
 * @method self countryId(string $value)
 * @method self regionDescr(string $value)
 * @method self districtDescr(string $value)
 * @method self cityDescr(string $value)
 * @method self addressDescr(string $value)
 * @method self notation(string $value)
 * @method self branchIdExternal(string $value)
 *
 * @method self streetName(string $value)
 * */
class Receiver extends BaseRequest
{

}
