<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Requests\Deep;

use Chicory\Entities\Parcels\Requests\BaseRequest;

/**
 * @method self article(string $value)
 * @method self name(string $value)
 * @method self serialNumber(string $value)
 * @method self weight(float $value)
 * @method self quantity(int $value)
 * @method self price(float $value)
 * @method self length(float $value)
 * @method self width(float $value)
 * @method self height(float $value)
 * */
class Good extends BaseRequest
{

}
