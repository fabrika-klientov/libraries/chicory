<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $parcelNumber
 * @property-read string $parcelCode
 * @property-read string $senderBranch
 * @property-read string $senderBranchNumber
 * @property-read string $receiverBranch
 * @property-read string $receiverBranchNumber
 * @property-read string $senderBranchTMO
 * @property-read string $senderBranchTMONumber
 * @property-read string $receiverBranchTMO
 * @property-read string $receiverBranchTMONumber
 * @property-read string $receiverService
 * @property-read array $statuses
 * */
class ParcelStatusDetails extends Entity implements BeEntity
{
    /** TODO: upd
     * @return array
     */
    public function statuses(): array
    {
        return $this->statuses; // self::collectFor('statuses', ::class);
    }
}
