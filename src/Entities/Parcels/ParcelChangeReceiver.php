<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;

/**
 * @property-read string $StatusCode
 * @property-read string $ErrorText
 * @property-read string $parcelID
 * @property-read string $parcelIDref
 * @property-read string $parcelNumber
 * @property-read string $docID
 * */
class ParcelChangeReceiver extends Entity implements BeEntity
{

}
