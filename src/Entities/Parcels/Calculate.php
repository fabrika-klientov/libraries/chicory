<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\PossibleDeliveryDate;

/**
 * @property-read string $costServices
 * @property-read string $estimatedDeliveryDate
 * @property-read array $possibleDeliveryDates
 * */
class Calculate extends Entity implements BeEntity
{
    /**
     * @return PossibleDeliveryDate[]
     */
    public function possibleDeliveryDates(): array
    {
        return self::collectFor('possibleDeliveryDates', PossibleDeliveryDate::class);
    }
}
