<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\ConditionDescr;

/**
 * @property-read string $conditionID
 * @property-read array $conditionDescr
 * */
class SpecCondition extends Entity implements BeEntity
{
    public function conditionDescr(): ?ConditionDescr
    {
        return empty($this->conditionDescr) ? null : new ConditionDescr($this->conditionDescr);
    }
}
