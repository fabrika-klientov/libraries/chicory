<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels;

use Chicory\Contracts\BeEntity;
use Chicory\Entities\Parcels\Additional\Good;

/**
 * @property-read string $parcelNumber
 * @property-read array $goods
 * */
class ParcelGoodsReturn extends Entity implements BeEntity
{
    /**
     * @return Good[]
     */
    public function goods(): array
    {
        return self::collectFor('goods', Good::class);
    }
}
