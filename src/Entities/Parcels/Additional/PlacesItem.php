<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Additional;

use Chicory\Entities\Parcels\Entity;

/**
 * @property-read string $quantity
 * @property-read string $weight
 * @property-read string $volume
 * @property-read string $insurance
 * @property-read string $packID
 * @property-read string $length
 * @property-read string $width
 * @property-read string $height
 * @property-read string $wheels
 * @property-read string $size
 * */
class PlacesItem extends Entity
{

}
