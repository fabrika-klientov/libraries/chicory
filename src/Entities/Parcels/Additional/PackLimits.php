<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Additional;

use Chicory\Entities\Parcels\Entity;

/**
 * @property-read float $weightMin
 * @property-read float $weightMax
 * @property-read float $volumeMin
 * @property-read float $volumeMax
 * @property-read float $lengthMax
 * @property-read float $widthMax
 * @property-read float $heightMax
 * */
class PackLimits extends Entity
{

}
