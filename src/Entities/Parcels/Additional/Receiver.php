<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Additional;

use Chicory\Entities\Parcels\Entity;
use Chicory\Entities\Search\Additional\CityDescr;
use Chicory\Entities\Search\Additional\CountryDescr;
use Chicory\Entities\Search\Additional\DistrictDescr;
use Chicory\Entities\Search\Additional\RegionDescr;

/**
 * @property-read string $name
 * @property-read string $phone
 * @property-read string $zipCode
 * @property-read string $branchID
 * @property-read string $branchDescr
 * @property-read string $addressID
 * @property-read string $addressDescr
 * @property-read string $building
 * @property-read string $flat
 * @property-read string $floor
 * @property-read string $service
 *
 * // additional (ex.: printCN23)
 * @property-read string $countryID
 * @property-read array $countryDescr
 * @property-read array $cityDescr
 * @property-read array $regionDescr
 * @property-read array $districtDescr
 * @property-read string $fullAddress
 * */
class Receiver extends Entity
{
    public function countryDescr(): ?CountryDescr
    {
        return empty($this->countryDescr) ? null : new CountryDescr($this->countryDescr);
    }

    public function cityDescr(): ?CityDescr
    {
        return empty($this->cityDescr) ? null : new CityDescr($this->cityDescr);
    }

    public function regionDescr(): ?RegionDescr
    {
        return empty($this->regionDescr) ? null : new RegionDescr($this->regionDescr);
    }

    public function districtDescr(): ?DistrictDescr
    {
        return empty($this->districtDescr) ? null : new DistrictDescr($this->districtDescr);
    }
}
