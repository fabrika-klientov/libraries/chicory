<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Additional;

use Chicory\Entities\Parcels\Entity;

/**
 * @property-read string $conditionID
 * @property-read string $conditionDescr
 * */
class SpecConditionsItem extends Entity
{

}
