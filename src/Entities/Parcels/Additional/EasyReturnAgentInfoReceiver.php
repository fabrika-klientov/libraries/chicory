<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.28
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Entities\Parcels\Additional;

use Chicory\Entities\Parcels\Entity;

/**
 * @property-read string $service
 * @property-read string $phone
 * @property-read string $name
 * @property-read string $branchID
 * @property-read string $addressId
 * @property-read string $countryId
 * @property-read string $regionDescr
 * @property-read string $districtDescr
 * @property-read string $cityDescr
 * @property-read string $building
 * @property-read string $flat
 * */
class EasyReturnAgentInfoReceiver extends Entity
{

}
