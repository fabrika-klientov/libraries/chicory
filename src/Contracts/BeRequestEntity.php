<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Contracts;

interface BeRequestEntity
{
    public function getData(): array;

    /**
     * @return bool
     *
     * @throws \Chicory\Exceptions\ValidateRequestException
     */
    public function validate(): bool;
}
