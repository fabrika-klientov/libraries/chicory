<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Exceptions;

use Exception;
use Throwable;

class ChicoryException extends Exception
{
    /**
     * @var array $dataError
     * */
    protected $dataError = [];

    /**
     * @var string $logPrefix
     * */
    protected static $logPrefix = 'CHICORY::CORE: ';

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $data
     * @return void
     */
    public function setDataError($data)
    {
        $this->dataError = $data ?? [];
    }

    /**
     * @return array
     */
    public function getDataError()
    {
        return $this->dataError;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = static::$logPrefix . str_replace(static::$logPrefix, '', $message);
    }
}
