<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Core\Http;

use Chicory\Core\Auth\AuthService;
use Chicory\Exceptions\AuthException;
use Chicory\Exceptions\HttpClientException;
use Chicory\Exceptions\NotFoundException;
use Chicory\Exceptions\ResponseErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Promise\Each;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class HttpClient
{
    public const API_URL = 'https://api.meest.com/%s/openAPI/';
    public const API_PUBLIC_URL = 'https://publicapi.meest.com/';
    public const API_VER = 'v3.0';

    protected const TIMEOUT = 30;
    public const CONCURRENCY = 10;

    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const PATCH = 'PATCH';
    public const DELETE = 'DELETE';

    /**
     * @var AuthService $authService
     * */
    private $authService;

    /**
     * @var bool $withAuth
     * */
    private $withAuth;

    /**
     * @param AuthService $authService
     * @param bool $withAuth
     */
    public function __construct(AuthService $authService, bool $withAuth = true)
    {
        $this->authService = $authService;
        $this->withAuth = $withAuth;
        $this->authService->setHttpClient($this);
    }

    /**
     * @param bool $withAuth
     */
    public function setWithAuth(bool $withAuth): void
    {
        $this->withAuth = $withAuth;
    }

    /** api GET
     * @param string $link
     * @param array $query
     * @param array $options
     * @param bool $async
     * @return array|RequestPack|null
     * @throws HttpClientException
     */
    public function get(string $link, array $query = [], array $options = [], bool $async = false)
    {
        $options = array_merge($options, ['query' => $query]);

        if ($async) {
            return new RequestPack(new Request(self::GET, $this->getLink($link)), $options);
        }

        return $this->request(self::GET, $link, $options);
    }

    /** api POST
     * @param string $link
     * @param array $data
     * @param array $options
     * @param bool $async
     * @return array|RequestPack|null
     * @throws HttpClientException
     */
    public function post(string $link, array $data, array $options = [], bool $async = false)
    {
        $options = array_merge($options, empty($data) ? [] : ['json' => $data]);

        if ($async) {
            return new RequestPack(new Request(self::POST, $this->getLink($link)), $options);
        }

        return $this->request(self::POST, $link, $options);
    }

    /** api PUT
     * @param string $link
     * @param array $data
     * @param array $options
     * @param bool $async
     * @return array|RequestPack|null
     * @throws HttpClientException
     */
    public function put(string $link, array $data, array $options = [], bool $async = false)
    {
        $options = array_merge($options, empty($data) ? [] : ['json' => $data]);

        if ($async) {
            return new RequestPack(new Request(self::PUT, $this->getLink($link)), $options);
        }

        return $this->request(self::PUT, $link, $options);
    }

    /** api DELETE
     * @param string $link
     * @param array $query
     * @param array $options
     * @param bool $async
     * @return array|RequestPack|null
     * @throws HttpClientException
     */
    public function delete(string $link, array $query = [], array $options = [], bool $async = false)
    {
        $options = array_merge($options, ['query' => $query]);

        if ($async) {
            return new RequestPack(new Request(self::DELETE, $this->getLink($link)), $options);
        }

        return $this->request(self::DELETE, $link, $options);
    }

    /**
     * @param bool $withAuth
     * @return Client
     * @throws AuthException
     */
    public function getClient(bool $withAuth = true): Client
    {
        $baseUri = self::getLink('');

        $options = [
            'base_uri' => $baseUri,
            'headers' => [
                'Accept' => 'application/json',
            ],
            'timeout' => self::TIMEOUT,
        ];

        if ($withAuth) {
            $options = self::injectAuth($options);
        }

        return new Client($options);
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param int $prevCode
     * @return array|null
     * @throws HttpClientException|AuthException
     * */
    protected function request(string $method, string $link, array $options, int $prevCode = 0)
    {
        try {
            $response = $this
                ->getClient($this->withAuth)
                ->request($method, $this->getLink($link), self::prepareOptions($method, $link, $options));

            $content = $response->getBody()->getContents();
            $result = json_decode($content, true);

            return $result;
        } catch (ClientException $e) {
            $code = $e->getCode();
            switch ($code) {
                case 401:
                    if ($this->withAuth && $prevCode != $code) {
                        $this->authService->loadToken(null, true);
                        return $this->request($method, $link, $options, $code);
                    }
                    break;
                case 404:
                    throw new NotFoundException('Data not found. ' . $e->getMessage(), $code);
                case 400:
                    $exception = new ResponseErrorException('Response error. ' . $e->getMessage(), $code);
                    $exception->setDataError(json_decode($e->getResponse()->getBody()->getContents() ?: '[]', true));

                    throw $exception;
            }

            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $code);
        } catch (ServerException $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        } catch (\Throwable $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param RequestPack[] $requests
     * @param int $concurrency
     * @param int $prevCode
     * @return array|null
     * @throws HttpClientException|AuthException
     * */
    public function requestAsync(array $requests, int $concurrency = self::CONCURRENCY, int $prevCode = 0)
    {
        try {
            $client = $this->getClient($this->withAuth);

            $iterator = function () use ($client, $requests) {
                foreach ($requests as $request) {
                    yield $client
                        ->sendAsync($request->getRequest(), $request->getOptions())
                        ->then(function (Response $response) use ($request) {
                            return [$request, $response];
                        });
                }
            };

            $results = [];
            $promise = Each::ofLimit(
                $iterator(),
                $concurrency,
                function ($result, $index) use (&$results) {
                    /**
                     * @var Request $request
                     * @var Response $response
                     */
                    list($request, $response) = $result;

                    $results[$index] = json_decode($response->getBody()->getContents(), true);
                }
            );

            $promise->wait();

            return $results;
        } catch (ClientException $e) {
            $code = $e->getCode();
            switch ($code) {
                case 401:
                    if ($this->withAuth && $prevCode != $code) {
                        $this->authService->loadToken(null, true);
                        return $this->requestAsync($requests, $concurrency, $code);
                    }
                    break;
                case 404:
                    throw new NotFoundException('Data not found. ' . $e->getMessage(), $code);
                case 400:
                    $exception = new ResponseErrorException('Response error. ' . $e->getMessage(), $code);
                    $exception->setDataError(json_decode($e->getResponse()->getBody()->getContents() ?: '[]', true));

                    throw $exception;
            }

            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $code);
        } catch (ServerException $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        } catch (\Throwable $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $link
     * @return string
     */
    public function getLink(string $link = ''): string
    {
        $uri = $this->withAuth ? sprintf(self::API_URL, self::API_VER) : self::API_PUBLIC_URL;

        if ($link) {
            $link = preg_replace('/^\//', '', $link);
            $uri .= $link;
        }

        return $uri;
    }

    /**
     * @deprecated
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array
     */
    protected function prepareOptions(string $method, string $link, array $options): array
    {
        // prepare headers
        $headers = $options['headers'] ?? [];
        switch ($method) {
            case self::POST:
            case self::PUT:
            case self::PATCH:
                if (empty($headers['Content-Type'])) {
                    $headers['Content-Type'] = 'application/json';
                }
                break;
        }
        $options['headers'] = $headers;

        return $options;
    }

    /**
     * @param array $options
     * @return array
     * @throws AuthException
     */
    protected function injectAuth(array $options): array
    {
        $headers = $options['headers'] ?? [];
        $headers['token'] = $this->authService->loadToken();
        $options['headers'] = $headers;

        return $options;
    }
}
