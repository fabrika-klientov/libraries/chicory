<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2022 Fabrika-Klientov
 * @version   GIT: 22.02.07
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Core\Http;

use GuzzleHttp\Psr7\Request;

class RequestPack
{
    private $request;
    private $options;

    public function __construct(Request $request, array $options)
    {
        $this->request = $request;
        $this->options = $options;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
