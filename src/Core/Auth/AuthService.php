<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Core\Auth;

use Chicory\Contracts\BeAuthAdapter;
use Chicory\Core\Auth\Adapters\AuthFileJsonAdapter;
use Chicory\Core\Http\HttpClient;
use Chicory\Exceptions\AuthException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class AuthService
{
    protected const AUTH = 'auth';
    protected const REFRESH = 'refreshToken';

    private $credentials;
    private $httpClient;
    /**
     * @var BeAuthAdapter $adapter
     * */
    private $adapter;

    private $token;
    private $refreshToken;
    private $expiresIn;

    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
        $this->token = $credentials->getToken();
        $this->refreshToken = $credentials->getRefreshToken();
    }

    /**
     * @param BeAuthAdapter $adapter
     */
    public function setAdapter(BeAuthAdapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    /**
     * @return Credentials
     */
    public function getCredentials(): Credentials
    {
        return $this->credentials;
    }

    /**
     * @param HttpClient $httpClient
     */
    public function setHttpClient(HttpClient $httpClient): void
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @throws AuthException
     */
    public function auth(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient, self::AUTH);
    }

    /**
     * @throws AuthException
     */
    public function refreshToken(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient);
    }

    /**
     * @throws AuthException
     */
    public function loadToken(HttpClient $httpClient = null, bool $force = false): string
    {
        if ($force) {
            try {
                $this->refreshToken($httpClient);
            } catch (AuthException $e) {
                switch ($e->getCode()) {
                    // try to auth
                    case AuthException::VALIDATE_AUTH_CODE:
                        $this->auth($httpClient);
                        break;

                    default:
                        throw $e;
                }
            }

            return $this->token;
        }

        if (empty($this->token)) {
            $authData = self::getAdapter()->read($this->credentials->getLogin());
            if (empty($authData['token'])) {
                return $this->loadToken($httpClient, true);
            }

            self::setTokens($authData);
        }

        return $this->token;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @throws AuthException
     */
    protected function doRequest(?HttpClient $httpClient, string $link = self::REFRESH)
    {
        $httpClient = self::checkHttpClientWithThrow($httpClient);
        $guzzleClient = $httpClient->getClient(false);

        switch ($link) {
            case self::AUTH:
                if (!$this->credentials->isLoginAuth()) {
                    throw new AuthException(
                        'For auth [login] and [password] are required',
                        AuthException::VALIDATE_AUTH_CODE
                    );
                }
                $json = ['username' => $this->credentials->getLogin(), 'password' => $this->credentials->getPassword()];
                break;

            case self::REFRESH:
                if (!$this->credentials->isRefreshTokenAuth()) {
                    throw new AuthException(
                        'For refresh token [refreshToken] is required',
                        AuthException::VALIDATE_AUTH_CODE
                    );
                }
                $json = ['refreshToken' => $this->refreshToken];
                break;

            default:
                throw new AuthException("Unexpected link [$link].", AuthException::VALIDATE_AUTH_CODE);
        }

        try {
            $response = $guzzleClient->post($link, ['json' => $json]);

            $contents = $response->getBody()->getContents();
            $result = json_decode($contents, true);

            if (empty($result['status']) || $result['status'] != 'OK') {
                throw new AuthException('Server returned not [OK] status');
            }

            if (empty($result['result'])) {
                throw new AuthException('Server returned empty data');
            }

            self::setTokens($result['result']);
            self::getAdapter()->write($this->credentials->getLogin(), $result['result']);

            return $result['result'];
        } catch (ClientException $e) {
            if ($link == self::REFRESH) {
                return self::doRequest($httpClient, self::AUTH);
            }

            throw new AuthException($e->getMessage(), $e->getCode());
        } catch (ServerException | GuzzleException $e) {
            throw new AuthException($e->getMessage(), $e->getCode());
        }
    }

    protected function setTokens(array $data)
    {
        $this->credentials->setToken($this->token = $data['token']);
        $this->credentials->setRefreshToken($this->refreshToken = $data['refreshToken']);
        $this->expiresIn = $data['expiresIn'] ?? null;
    }

    /**
     * @throws AuthException
     */
    protected function checkHttpClientWithThrow(?HttpClient $httpClient): HttpClient
    {
        $httpClient = $httpClient ?? $this->httpClient;
        if (empty($httpClient)) {
            throw new AuthException('You should set HttpClient before get request');
        }

        return $httpClient;
    }

    /**
     * @return BeAuthAdapter
     */
    protected function getAdapter(): BeAuthAdapter
    {
        if (empty($this->adapter)) {
            $this->adapter = new AuthFileJsonAdapter();
        }

        return $this->adapter;
    }
}
