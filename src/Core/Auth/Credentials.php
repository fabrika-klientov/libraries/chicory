<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Core\Auth;

class Credentials
{
    /**
     * @var string|null $login
     * */
    private $login;
    /**
     * @var string|null $password
     * */
    private $password;
    /**
     * @var string|null $token
     * */
    private $token;
    /**
     * @var string|null $refreshToken
     * */
    private $refreshToken;

    public function __construct(
        string $login = null,
        string $password = null,
        string $token = null,
        string $refreshToken = null
    ) {
        $this->login = $login;
        $this->password = $password;
        $this->token = $token;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    /**
     * @param string|null $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return bool
     */
    public function isLoginAuth(): bool
    {
        return !empty($this->login) && !empty($this->password);
    }

    /**
     * @return bool
     */
    public function isTokenAuth(): bool
    {
        return !empty($this->token);
    }

    /**
     * @return bool
     */
    public function isRefreshTokenAuth(): bool
    {
        return !empty($this->refreshToken);
    }
}
