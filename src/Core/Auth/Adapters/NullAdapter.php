<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory\Core\Auth\Adapters;

use Chicory\Contracts\BeAuthAdapter;

class NullAdapter implements BeAuthAdapter
{
    public function read(string $login): array
    {
        return [];
    }

    public function write(string $login, array $data): void
    {
        //
    }
}
