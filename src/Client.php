<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Chicory
 * @category  Chicory
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.27
 * @link      https://fabrika-klientov.ua
 */

namespace Chicory;

use Chicory\Contracts\BeAuthAdapter;
use Chicory\Core\Auth\AuthService;
use Chicory\Core\Auth\Credentials;
use Chicory\Core\Http\HttpClient;
use Chicory\Services;

class Client
{
    private $authService;
    private $httpClient;

    public function __construct(Credentials $credentials)
    {
        $this->authService = new AuthService($credentials);
        $this->httpClient = new HttpClient($this->authService);
    }

    /**
     * @param BeAuthAdapter $authAdapter
     * @return Client
     */
    public function withAuthAdapter(BeAuthAdapter $authAdapter): Client
    {
        $this->authService->setAdapter($authAdapter);

        return $this;
    }

    /**
     * @return AuthService
     */
    public function getAuthService(): AuthService
    {
        return $this->authService;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

    // additional services

    /**
     * @return Services\SearchService
     */
    public function getSearchService(): Services\SearchService
    {
        return new Services\SearchService($this);
    }

    /**
     * @return Services\ParcelsService
     */
    public function getParcelsService(): Services\ParcelsService
    {
        return new Services\ParcelsService($this);
    }

    /**
     * @return Services\RegistersService
     */
    public function getRegistersService(): Services\RegistersService
    {
        return new Services\RegistersService($this);
    }

    /**
     * @return Services\PrintService
     */
    public function getPrintService(): Services\PrintService
    {
        return new Services\PrintService($this);
    }

    /**
     * @return Services\TrackingService
     */
    public function getTrackingService(): Services\TrackingService
    {
        return new Services\TrackingService($this);
    }

    /**
     * @return Services\OtherService
     */
    public function getOtherService(): Services\OtherService
    {
        return new Services\OtherService($this);
    }

    // services without requiring auth

    /**
     * @return Services\BranchesService
     */
    public function getPublicBranchesService(): Services\BranchesService
    {
        return new Services\BranchesService($this);
    }

    /**
     * @return Services\GeoService
     */
    public function getPublicGeoService(): Services\GeoService
    {
        return new Services\GeoService($this);
    }
}
