## Status
       
[![Latest Stable Version](https://poser.pugx.org/fbkl/chicory/v/stable)](https://packagist.org/packages/fbkl/chicory)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/chicory/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/chicory/commits/master)
[![License](https://poser.pugx.org/fbkl/chicory/license)](https://packagist.org/packages/fbkl/chicory)




**Клиент для работы с API Meest Express (Meest)**

- [Документация](https://wiki.meest-group.com/api/ua/v3.0/openAPI)
- [Документация public api](https://publicapi.meest.com/)

---

## Usage

`composer require fbkl/chicory`

---

## Работа с библиотекой

### Client (Клиент для работы с Meest)

Для начала работы с библиотекой Meest необходимо создать инстанс клиента, передав ему 
ваши авторизационные данные

1. Создайте объект `Credentials`:
```injectablephp
use Chicory\Core\Auth\Credentials;

// Все параметры опциональны
$credentials = new Credentials('login', 'password');

// или
$credentials = new Credentials();
$credentials->setLogin('login');
$credentials->setPassword('password');

```

2. Создайте клиент `Client`
```injectablephp
use Chicory\Client;

$client = new Client($credentials);

```

**Методы используют `token` (oauth) для запросов.
По умолчанию данные авторизации хранятся в файлах (с названиями логинов клиентов)
в директории `storage/auth/chicory/` (с помощью `AuthFileJsonAdapter`).
Есть возможность изменить поведение передав свой адаптер реализовав интерфейс
`BeAuthAdapter`.**

```injectablephp
use Chicory\Client;

class MyAdapter implements \Chicory\Contracts\BeAuthAdapter
{
    public function read(string $login): array
    {
        // todo ....
        return ['token' => '...', 'refreshToken' => '...'];
    }
    
    public function write(string $login, array $data): void
    {
        // todo save
    }
}

$client = new Client($credentials);
$client->withAuthAdapter(new MyAdapter());
```

3. С помощью клиента вы можете создать (вызвать) нужный сервис для работы:
```injectablephp
$service1 = $client->getParcelsService();
$service2 = $client->getPublicBranchesService();
$service3 = $client->getTrackingService();
// ...
```

### Services

Доступные сервисы:
- Требующие авторизацию:
  - `SearchService` - Поиск админ. тер. едениц
  - `ParcelsService` - Работа с отправлениями
  - `RegistersService` - Работа с Реестрами 
  - `PrintService` - Работа с печатными формами
  - `TrackingService` - Работа с трекингом
  - `OtherService` - Другие методы
- Без авторизации:
  - `BranchesService` - Работа с поиском отделений
  - `GeoService` - Работа с гео данными (области, районы, нас. пункты, улицы)


---

Во время работы с сервисами - все запросы строятся с помощью
объекта конструктора запросов на вход необходимого метода сервиса
(если эти данные нужны методу).

Результат выполнения будет всегда объект (сущность) определенного класса
или коллекция этих объектов (сущностей), за исключением `PrintService` -
в результате ссылки формы (строка).

`TODO: По надобности расписать работу по каждому сервису и его методах отдельно`

### Service SearchService

...

### Service ParcelsService

...

---

### Create Parcels

```injectablephp
use Chicory\Entities\Parcels\Requests\CreateParcelRequest;
use Chicory\Entities\Parcels\Requests\Deep\Sender;
use Chicory\Entities\Parcels\Requests\Deep\Receiver;
use Chicory\Entities\Parcels\Requests\Deep\Good;
use Chicory\Entities\Registers\Requests\RegisterBranchRequest;
use Chicory\Entities\Registers\Requests\Deep\ParcelsItem;

// Получает сервис работы с посылками
$parcelsService = $client->getParcelsService();

// Создаем запрос на создание отправления
$request = new CreateParcelRequest();
$request
    ->parcelNumber('TST-2550636')
    ->payType(CreateParcelRequest::PAY_TYPE_CASH)
    ->receiverPay(false)
    ->sender(
        (new Sender())
            ->name('Петро')
            ->phone('+380999999999')
            ->branchID('2787a594-22f4-11e9-80dc-1c98ec135261')
            ->service(CreateParcelRequest::SERVICE_BRANCH)
//            ->addressID('681d4711-e0d2-11df-9b37-00215aee3ebe')
//            ->building('1')
//            ->flat('1')
    )
    ->receiver(
        (new Receiver())
            ->name('Иван')
            ->phone('+380988888888')
            ->branchID('9e924578-7f1d-11ea-80c6-000c29800ae7')
            ->service(CreateParcelRequest::SERVICE_BRANCH)
    )
    ->weight(10)
    ->goods([
        (new Good())
            ->article('12345')
            ->name('mask')
            ->weight(4)
            ->quantity(1)
            ->price(112)
    ]);

try {
    // Создаем отправление
    $result = $client->getParcelsService()->createParcel($request);
} catch (\Chicory\Exceptions\ResponseErrorException $exception) {
    //
}

// Добавляем отправление в регистр
$request2 = new RegisterBranchRequest();
$request2
    ->notation('note')
    ->parcelsItems([
        (new ParcelsItem())
            ->parcelID($result->result()->parcelID),
    ]);
$result2 = $client->getRegistersService()->createRegisterBranch($request);

```

